<h1>How has this project been created?</h1>

<h2>Which method did I use?</h2>
I decided to create that project using Symfony.
In the correct folder, 

<em>symfony new --full application_cda</em>

and it downloaded all the necessary files.

<h2>What to do then?</h2>

Then, i needed a database to store datas. To do so, i updated the .env file to put my login + password , and a database name.
I used, then , 

<em>php bin/console doctrine:database:create</em>

and my database has been created.

<h2>How to fill that database?</h2>

Thanks to the diagram given, I created entities thanks to 

<em>php bin/console make:entity</em>

and created all my entities.

Thanks to relations on the diagram, I could create foreign keys for each table.

Once all the properties were added, I executed those 2 commands:

 <em>php bin/console make:migration</em><br>
 <em>php bin/console doctrine:migrations:migrate</em> 
 
 to create my database tables.
 
 <h2>How to store/edit/show/delete datas?</h2>

 To store/edit/show/delete datas, I needed a controller.
 Thanks to 
 
 <em>php bin/console make:crud EntityName</em>
 
 It created all what i needed. The controller (with all functions needed), form type and all twig templates.
 
 I finally updated the file "routes.yaml" to change homepage URL.
 
